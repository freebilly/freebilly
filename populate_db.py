from app import db, models
from datetime import datetime

authors = [
    "Phillip Pullman",
    "Cixin Liu"
]

books = [
    {
        "author_id": 2,
        "date_added": "2020-12-16 13:55:21.055098",
        "image_url": "https://www.bibdsl.co.uk/imagegallery2/publisher/batch1961/9781788543002.JPG",
        "isbn13": "978-7-5366-9293-0",
        "title": "The Three-Body Problem"
    },
    {
        "author_id": 2,
        "date_added": "2020-12-16 13:55:39.744792",
        "image_url": "https://www.bibdsl.co.uk/imagegallery2/publisher/batch1961/9781788543019.JPG",
        "isbn13": "978-1784971595",
        "title": "The Dark Forest"
    },
    {
        "author_id": 2,
        "date_added": "2020-12-16 13:55:53.583762",
        "image_url": "https://www.bibdsl.co.uk/imagegallery2/BDS/201835/9781788543026.jpg",
        "isbn13": "978-0765377104",
        "title": "Death's End"
    },
    {
        "author_id": 1,
        "date_added": "2020-12-16 13:56:03.694407",
        "image_url": "http://www.socialbookshelves.com/wp-content/uploads/2015/11/his-dark-materials-northern-lights.jpg",
        "isbn13": "0-590-54178-1",
        "title": "Northern Lights"
    },
    {
        "author_id": 1,
        "date_added": "2020-12-16 13:56:15.013661",
        "image_url": "https://pictures.abebooks.com/SIAMESE1/21533676193_2.jpg",
        "isbn13": "0-590-54243-5",
        "title": "The Subtle Knife"
    },
    {
        "author_id": 1,
        "date_added": "2020-12-16 13:56:24.958932",
        "image_url": "http://www.socialbookshelves.com/wp-content/uploads/2015/11/a-la-croisee-des-mondes-tome-3-le-miroir-d-ambre-307255.jpg",
        "isbn13": "0-590-54244-3",
        "title": "The Amber Spyglass"
    }
]

collections = [
    {
        "author_id": 1,
        "books": [4, 5, 6],
        "image_url": "https://4.bp.blogspot.com/-7a3-FNukl98/TtjiEDMtxII/AAAAAAAAAPw/MqHuMkMUF1Y/s1600/His%2BDark%2BMaterials%2Bbook%2Bjacket%2B2.jpg",
        "title": "His Dark Materials"
    },
    {
        "author_id": 2,
        "books": [1, 2, 3],
        "image_url": "https://www.bibdsl.co.uk/imagegallery2/publisher/batch1961/9781788543002.JPG",
        "title": "Remembrance of Earth's Past"
    }
]

models.Author.query.delete()
models.Book.query.delete()
models.Collection.query.delete()
db.session.commit()

for author in authors:
    a = models.Author(name=author)
    db.session.add(a)

db.session.commit()

for book in books:
    b = models.Book(
        author=models.Author.query.get(book['author_id']),
        date_added=datetime.strptime(book['date_added'], '%Y-%m-%d %H:%M:%S.%f'),
        image_url=book['image_url'],
        title=book['title']
    )
    db.session.add(b)

db.session.commit()


def is_in(a, b):
    return a in b


for collection in collections:
    books = models.Book.query.all()
    books = [book for book in books if is_in(book.id, collection['books'])]
    c = models.Collection(
        author=models.Author.query.get(collection['author_id']),
        books=books,
        image_url=collection['image_url'],
        title=collection['title']
    )
    db.session.add(c)

db.session.commit()
