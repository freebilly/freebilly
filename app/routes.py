from flask import request, send_from_directory

from app import app


@app.route('/')
def index():
    return send_from_directory('./static', 'index.html')


@app.route('/<path:path>')
def catch_all(path):
    return send_from_directory('./static', path)


@app.errorhandler(404)
def error_404(e):
    return 'This route does not exist {}'.format(request.url), 404
