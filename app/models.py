from __future__ import annotations
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String
)
from typing import Any, TYPE_CHECKING

from app import db

# Type definitions for mypy
if TYPE_CHECKING:
    BaseModel: Any = db.declarative_base()  # type: ignore
    db: Any = db  # type: ignore
else:
    BaseModel = db.Model


class Author(BaseModel):
    id = Column(Integer, primary_key=True)
    books = db.relationship('Book', lazy=True)
    collections = db.relationship('Collection', lazy=True)
    image_url = Column(String)
    name = Column(String, index=True)


class Book(BaseModel):
    id = Column(Integer, primary_key=True)
    author = db.relationship('Author', lazy=True, back_populates='books')
    author_id = Column(
        Integer,
        ForeignKey('author.id'),
        nullable=False
    )
    collection = db.relationship('Collection', lazy=True)
    collection_id = Column(
        Integer,
        ForeignKey('collection.id'),
        nullable=True
    )
    date_added = Column(DateTime, index=True)
    deleted = Column(Boolean)
    file_path = Column(String)
    image_url = Column(String)
    isbn = Column(String, unique=True)
    title = Column(String, index=True)


class Collection(BaseModel):
    id = Column(Integer, primary_key=True)
    author = db.relationship('Author', lazy=True, back_populates='collections')
    author_id = Column(
        Integer,
        ForeignKey('author.id'),
        nullable=False
    )
    books = db.relationship('Book', lazy=True, back_populates='collection')
    date_added = Column(DateTime, index=True)
    image_url = Column(String)
    title = Column(String, index=True)
