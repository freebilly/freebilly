from marshmallow import fields
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested

from app import app
from app.models import Author, Book, Collection


class PrefixImageUrl(fields.Field):
    """Field that serialises to a prefixed URL and de-serialises
    to a non-prefixed URL."""

    def _serialize(self, value, attr, obj, **kwargs):
        if value:
            return app.config['IMAGES_PATH'] + value
        else:
            return None

    def _deserialize(self, value, attr, data, **kwargs):
        if value:
            return value.removeprefix(app.config['IMAGES_PATH'])
        else:
            return None


class PrefixBookUrl(fields.Field):
    """Field that serialises to a prefixed URL and de-serialises
    to a non-prefixed URL."""

    def _serialize(self, value, attr, obj, **kwargs):
        return app.config['BOOKS_PATH'] + value

    def _deserialize(self, value, attr, data, **kwargs):
        return value.removeprefix(app.config['BOOKS_PATH'])


class AuthorSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Author

    image_url = PrefixImageUrl(attribute='image_url')


class BookSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Book

    author = Nested(
        'AuthorSchema',
        many=False
    )
    collection = Nested(
        'CollectionSchema',
        many=False,
        exclude=('books',)
    )
    file_path = PrefixBookUrl(attribute='file_path')
    image_url = PrefixImageUrl(attribute='image_url')


class CollectionSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Collection

    author = Nested(
        'AuthorSchema',
        many=False
    )
    books = Nested(
        'BookSchema',
        many=True,
        exclude=('collection.books',)
    )
    image_url = PrefixImageUrl(attribute='image_url')
