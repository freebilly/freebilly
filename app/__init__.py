from flask import Flask
from flask_compress import Compress
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow

app = Flask(
    __name__,
    static_folder='../static',
    static_url_path='/static'
)
Compress(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

from app.watcher import Watcher  # noqa: E402
watcher = Watcher()


from app import models, routes  # noqa: E402, F401
from app.api import blueprint as api  # noqa: E402
app.register_blueprint(api, url_prefix='/')

if __name__ == '__main__':
    app.run(
        debug=False,
        use_reloader=True
    )


# Columns cannot be modified in SQLite,
# so the database has to be rebuilt
with app.app_context():
    if db.engine.url.drivername == 'sqlite':
        migrate.init_app(app, db, render_as_batch=True)
    else:
        migrate.init_app(app, db)
