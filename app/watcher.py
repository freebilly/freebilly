import base64
import epub_meta
import isbnlib
from odict import odict
import os
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from app import app, db  # type: ignore
from app.models import Author, Book


class Watcher():
    _books_root = app.config['BOOKS_ROOT']  # type: ignore
    _images_root = app.config['IMAGES_ROOT']  # type: ignore

    def __init__(self):
        self._scan_fs()
        self._create_observer()

    def _delete_book(self, book: Book):
        """ Delete the book from the filesystem and
        remove it from the database """
        if book.file_path:
            os.remove(book.file_path)
        with app.app_context():
            db.session.delete(book)
            db.session.commit()

    def _extract_epub_cover_image(self, metadata, isbn: str) -> Path:
        """ Extracts the cover image from an ePub, saves it, and returns the
        saved location """
        data = metadata.cover_image_content
        if data:
            extension = metadata.cover_image_extension
            path = Path(f'{self._images_root}{isbn}{extension}')
            path.touch(exist_ok=True)
            with open(path, 'wb') as f:
                f.write(base64.decodebytes(data))
            return path.relative_to(self._images_root)
        else:
            return Path()

    def _get_author_create_if_new(self, name: str) -> Author:
        """ Correct the author's name if it is in the format
        "surname, forename". Does the author already exist in the database?
        If not, create them. """
        file_author = self._swap_names_if_comma_separated(name)
        with app.app_context():
            db_author = Author.query.filter(
                Author.name == file_author
            ).first()
            if db_author:
                author = db_author
            else:
                name = file_author
                author = Author(name)
                with app.app_context():
                    db.session.add(author)
                    db.session.commit()
        return author

    def _get_first_isbnlike(self, values: list[str]) -> str:
        """ Return the first instance of an ISBN-13 in a book's metadata.
        If there are none, return the corresponding ISBN-13 of the first
        ISBN-10 found. If there are no ISBN-10s or ISBN-13s, return None. """
        if values:
            isbns = [v for v in values if isbnlib.get_isbnlike(v)]
            isbn13s = [i for i in isbns if isbnlib.is_isbn13(i)]
            if isbn13s:
                return isbnlib.canonical(isbn13s[0])
            elif isbns:
                return isbnlib.to_isbn13(isbns[0])
        return ''

    def _get_relative_path(self, path) -> Path:
        """ Return the path relative to the books_root """
        return Path(path).relative_to(self._books_root)

    def _swap_names_if_comma_separated(self, name: str) -> str:
        """ If the given name is in the format "surname, forename", swap them
        to "forename surname" """
        pre, comma, post = name.partition(', ')
        if post:
            return post+' '+pre
        else:
            return name

    def _update_db_added_book(
        self,
        author: Author,
        image_path: Path,
        isbn: str,
        metadata: odict,
        path: Path
    ):
        """ Add a book to the database with the specified properties """
        book = Book(
            author=author,
            file_path=path.name,
            image_url=image_path.name,
            isbn=isbn,
            title=metadata['title']
        )
        with app.app_context():
            db.session.add(book)
            db.session.commit()
        print(f'ADDED: {path}')

    def _update_db_moved_book(self, book: Book, new_path: Path):
        """ Update a book's file path to the specified location """
        old_path = book.file_path
        book.file_path = str(new_path)  # type: ignore
        with app.app_context():
            db.session.add(book)
            db.session.commit()
        print(f'{old_path} MOVED TO {new_path}')

    def _update_db_deleted_book(self, book: Book):
        """ Mark a book as deleted """
        book.deleted = True  # type: ignore
        with app.app_context():
            db.session.add(book)
            db.session.commit()
        print(f'MARKED AS DELETED: {book.file_path}')

    def _create_observer(self):
        """ Create a Watchdog event handler and observer """
        event_handler = PatternMatchingEventHandler(
            patterns=['*.epub'],
            ignore_patterns=None,
            ignore_directories=True,
            case_sensitive=False
        )
        event_handler.on_created = self._on_created
        event_handler.on_deleted = self._on_deleted
        event_handler.on_modified = self._on_modified
        event_handler.on_moved = self._on_moved
        observer = Observer()
        observer.schedule(
            event_handler,
            self._books_root,
            recursive=True
        )
        observer.start()
        # observer.join()

    def _on_created(self, event):
        """ Fires when Watchdog detects that an ePub file has been added """
        # print(f'ADDED: {get_relative_path(event.src_path)}')
        self._process_added_book(self._get_relative_path(event.src_path))

    def _on_deleted(self, event):
        """ Fires when Watchdog detects that an ePub file has been deleted """
        print(f'DELETED: {self._get_relative_path(event.src_path)}')
        with app.app_context():
            book = Book.query.filter(
                Book.file_path == self._get_relative_path(event.src_path).name
            ).first()
        if book and not book.deleted:
            self._update_db_deleted_book(book)

    def _on_modified(self, event):
        """ Fires when Watchdog detects that an ePub file has been modified """
        print(f'MODIFIED: {self._get_relative_path(event.src_path)}')
        self._process_added_book(self._get_relative_path(event.src_path))

    def _on_moved(self, event):
        """ Fires when Watchdog detects that an ePub file has been moved """
        print(f'MOVED: {self._get_relative_path(event.src_path)}')
        with app.app_context():
            book = Book.query.filter(
                Book.file_path == self._get_relative_path(event.src_path).name
            ).first()
        if book:
            self._update_db_moved_book(
                book=book,
                new_path=self._get_relative_path(event.dest_path)
            )

    def _process_added_book(self, path: Path):
        """ Process an added ePub file by adding it to the database or updating
        its entry in the database if it already exists """
        try:
            metadata = epub_meta.get_epub_metadata(self._books_root+str(path))
            isbn = self._get_first_isbnlike(metadata.identifiers)

            # If the ePub metadata doesn't contain an ISBN,
            # find the book by its title
            if not isbn:
                isbn = isbnlib.isbn_from_words(metadata['title'])

            # If the ISBN is ISBN-10, get the associated ISBN-13
            if isbnlib.is_isbn10(isbn):
                isbn = isbnlib.to_isbn13(isbn)

            # Does the book already exist in the database?
            # If it does, alter the file_path.
            # If it doesn't, create it and its author
            with app.app_context():
                db_book = Book.query.filter(
                    Book.isbn == isbn
                ).first()
            if db_book:
                self._update_db_moved_book(book=db_book, new_path=path)
            else:
                author = self._get_author_create_if_new(
                    metadata['authors'][0]
                )
                image_path = self._extract_epub_cover_image(metadata, isbn)
                self._update_db_added_book(
                    author=author,
                    image_path=image_path,
                    isbn=isbn,
                    metadata=metadata,
                    path=path
                )
            # try:
            #     metadata = isbnlib.meta(isbn, service='goob')
            # except isbnlib.dev._exceptions.DataNotFoundAtServiceError:
            #     print('goob: DATA NOT FOUND')
            # except Exception as e:
            #     print(e)
        except epub_meta.exceptions.EPubException:
            # Ignore malformed ePub files
            print(f'IGNORED: {path}')

    def _process_deleted_book(self, path: Path):
        """ Process a deleted ePub file by marking it as deleted if it exists
        in the database but is not marked as deleted, or updating the book's
        path in the database if it can be found by its ISBN """
        with app.app_context():
            book = Book.query.filter(Book.file_path == path).first()
        # If the deleted path appears in the database, mark the associated book
        # as deleted/missing, so that it can be deleted at a later date if the
        # file is not added again in the meantime. If the deleted path does not
        # exist in the database, process it as a new or moved book.
        if book and not book.deleted:
            self._update_db_deleted_book(book)
        else:
            try:
                metadata = epub_meta.get_epub_metadata(self._books_root+path)
                isbn = self._get_first_isbnlike(metadata.identifiers)
                with app.app_context():
                    book = Book.query.filter(Book.isbn == isbn).first()
                # If the book's ISBN exists in the database, update the
                # file path.
                if book:
                    self._update_db_moved_book(book=book, new_path=path)
            except epub_meta.EPubException:
                pass

    def _scan_fs(self):
        """ Scan the file system and compare the resulting set of file paths
        with a set of file paths obtained from the database and process those
        paths as added or deleted books as appropriate """
        fs_paths = set(
            self._get_relative_path(p).name
            for p in Path(self._books_root).rglob('*.epub')
        )
        with app.app_context():
            db_paths = set(b.file_path for b in Book.query)
        added_paths = fs_paths.difference(db_paths)
        deleted_paths = db_paths.difference(fs_paths)
        for path in added_paths:
            self._process_added_book(Path(path))
        for path in deleted_paths:
            self._process_deleted_book(Path(path))
