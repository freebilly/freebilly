from flask import Blueprint, jsonify, request
from flask_restx import Api, Resource
from flask_sqlalchemy import BaseQuery
from marshmallow_sqlalchemy import SQLAlchemySchema
from sqlalchemy import desc, or_, text

from app import app, db  # type: ignore
from app.models import Author, Book, Collection
from app.schemas import AuthorSchema, BookSchema, CollectionSchema

author_schema = AuthorSchema()
book_schema = BookSchema()
collection_schema = CollectionSchema()

authors_schema = AuthorSchema(many=True)
books_schema = BookSchema(many=True)
collections_schema = CollectionSchema(many=True)

blueprint = Blueprint('api', __name__)
api = Api(blueprint)


def assemble_paginated_response(
        query: BaseQuery,
        schema: SQLAlchemySchema,
        limit: int,
        page: int
):
    paginated_query = query.paginate(
        page=page,
        per_page=limit
    )
    items_end = min(limit*paginated_query.page, paginated_query.total)
    return jsonify({
        'items': schema.dump(paginated_query.items),
        'items_end': items_end,
        'items_start': (limit*(paginated_query.page-1))+1,
        'items_total': paginated_query.total,
        'page': paginated_query.page,
        'pages': paginated_query.pages,
        'per_page': paginated_query.per_page
    })


@api.route('/api/authors')
class ApiAuthors(Resource):
    def get(self):
        query = db.session.query(Author)\
            .filter(Author.id != 0)

        # Handle arguments
        page = request.args.get('page', 1, int)
        limit = request.args.get('limit', app.config['RESULTS_PER_PAGE'], int)
        sort_order = request.args.get('sort_order', 'asc', str)

        if sort_order == 'desc':
            query = query.order_by(desc('name'))
        else:
            query = query.order_by('name')

        # Construct query and response
        return assemble_paginated_response(
            limit=limit,
            page=page,
            schema=authors_schema,
            query=query
        )


@api.route('/api/authors/<int:author_id>')
class ApiAuthor(Resource):
    def get(self, author_id):
        author = Author.query.get(author_id)
        return jsonify(author_schema.dump(author))


@api.route('/api/books')
class ApiBooks(Resource):
    def get(self):
        query = db.session.query(Book)\
            .filter(
                Book.author_id == Author.id,
                Book.collection_id == Collection.id,
                Book.deleted == False  # noqa: E712
            )

        # Handle arguments
        page = request.args.get('page', 1, int)
        limit = request.args.get('limit', app.config['RESULTS_PER_PAGE'], int)
        author_id = request.args.get('author_id', None, int)
        sort_by = request.args.get('sort_by', 'title', str)
        sort_order = request.args.get('sort_order', 'asc', str)

        if author_id:
            query = query.filter(Book.author_id == author_id)

        if sort_by in ['isbn']:
            order_property = sort_by
        elif sort_by == 'author':
            order_property = text('author.name')
        elif sort_by == 'collection':
            order_property = text('collection.title')
        elif sort_by == 'date_added':
            order_property = text('book.date_added')
        else:
            order_property = text('book.title')

        if sort_order == 'desc':
            query = query.order_by(desc(order_property))
        else:
            query = query.order_by(order_property)

        # Construct query and response
        return assemble_paginated_response(
            limit=limit,
            page=page,
            schema=books_schema,
            query=query
        )


@api.route('/api/books/<int:book_id>')
class ApiBook(Resource):
    def get(self, book_id):
        book = Book.query.get(book_id)
        return jsonify(book_schema.dump(book))


@api.route('/api/collections')
class ApiCollections(Resource):
    def get(self):
        query = db.session.query(Collection)\
            .filter(
                Collection.author_id == Author.id,
                Collection.id != 0
            )

        # Handle arguments
        page = request.args.get('page', 1, int)
        limit = request.args.get('limit', app.config['RESULTS_PER_PAGE'], int)
        author_id = request.args.get('author_id', None, int)
        sort_by = request.args.get('sort_by', 'title', str)
        sort_order = request.args.get('sort_order', 'asc', str)

        if author_id:
            query = query.filter(Collection.author_id == author_id)

        if sort_by == 'author':
            order_property = text('author.name')
        elif sort_by == 'date_added':
            order_property = text('collection.date_added')
        else:
            order_property = text('collection.title')

        if sort_order == 'desc':
            query = query.order_by(desc(order_property))
        else:
            query = query.order_by(order_property)

        # Construct query and response
        return assemble_paginated_response(
            limit=limit,
            page=page,
            schema=collections_schema,
            query=query
        )


@api.route('/api/collections/<int:collection_id>')
class ApiCollection(Resource):
    def get(self, collection_id):
        collection = Collection.query.get(collection_id)
        data = collection_schema.dump(collection)
        data['books'] = [book for book in data['books'] if not book['deleted']]
        return jsonify(data)


@api.route('/search')
class Search(Resource):
    def get(self):
        query_string = request.args.get('q', None, str)
        filter_by = request.args.get('filter_by', None, str)
        page = request.args.get('page', 1, int)
        limit = request.args.get('limit', app.config['RESULTS_PER_PAGE'], int)

        if query_string:
            if filter_by == 'author':
                query = Author.query\
                    .filter(
                        Author.name.like(f'%{query_string}%')
                    )\
                    .order_by(Author.name)
                schema = authors_schema
            elif filter_by == 'book':
                query = Book.query.join(Author)\
                    .filter(
                        or_(
                            Book.title.like(f'%{query_string}%'),
                            Author.name.like(f'%{query_string}%')
                        )
                    )\
                    .order_by(Book.title)
                schema = books_schema
            elif filter_by == 'collection':
                query = Collection.query\
                    .join(Author)\
                    .filter(
                        or_(
                            Collection.title.like(f'%{query_string}%'),
                            Author.name.like(f'%{query_string}%')
                        )
                    )\
                    .order_by(Collection.title)
                schema = collections_schema

            return assemble_paginated_response(
                query=query,
                schema=schema,
                limit=limit,
                page=page
            )
