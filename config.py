import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # Admin settings
    BOOKS_ROOT = './static/books/'
    IMAGES_ROOT = './static/images/'
    RESULTS_PER_PAGE = 36

    # Exposed paths
    BOOKS_PATH = '/static/books/'
    IMAGES_PATH = '/static/images/'

    # For flask_compress
    COMPRESS_MIMETYPES = [
        'text/html',
        'text/css',
        'application/json',
        'application/javascript',
    ]

    # For Flask-SQLAlchemy
    SECRET_KEY = os.environ.get('SECRET_KEY') or \
        'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
