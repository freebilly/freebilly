import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouterReferer from '@tozd/vue-router-referer'
import VueI18n from 'vue-i18n'

import JSZip from 'jszip'
import ePub from 'epubjs'

Vue.use(Buefy)
Vue.use(VueAxios, axios)
Vue.use(VueRouterReferer)
Vue.use(VueI18n)
Vue.use(JSZip)
Vue.use(ePub)

Vue.config.productionTip = false

import { languages, defaultLocale, fallbackLocale } from './i18n/index.js';
const messages = Object.assign(languages)
const i18n = new VueI18n({
  locale: defaultLocale,
  fallbackLocale: fallbackLocale,
  messages
})

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
