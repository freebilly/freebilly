import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/author/:id',
    name: 'Author',
    component: () => import('../views/Author.vue'),
    props: true
  },
  {
    path: '/author/:id/books',
    name: 'AuthorBooks',
    component: () => import('../views/Author/Books.vue'),
    props: true
  },
  {
    path: '/author/:id/collections',
    name: 'AuthorCollections',
    component: () => import('../views/Author/Collections.vue'),
    props: true
  },
  {
    path: '/authors',
    name: 'Authors',
    component: () => import('../views/Authors.vue')
  },
  {
    path: '/book/:id',
    name: 'Book',
    component: () => import('../views/Book.vue'),
    props: true
  },
  {
    path: '/books',
    name: 'Books',
    component: () => import('../views/Books.vue')
  },
  {
    path: '/collection/:id',
    name: 'Collection',
    component: () => import('../views/Collection.vue'),
    props: true
  },
  {
    path: '/collections',
    name: 'Collections',
    component: () => import('../views/Collections.vue')
  },
  {
    path: '/metadatamanager',
    name: 'MetadataManager',
    component: () => import('../views/MetadataManager.vue')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue')
  },
  {
    path: '/viewbook/:id',
    name: 'ViewBook',
    component: () => import('../views/ViewBook.vue'),
    props: true
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
