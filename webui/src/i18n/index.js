import en_GB from './en_GB.yaml'
import fr_FR from './fr_FR.yaml'

export const defaultLocale = 'en_GB'
export const fallbackLocale = 'en_GB'

export const languages = {
  en_GB: en_GB,
  fr_FR: fr_FR
}

export const locales = require.context(
  '@/i18n',
  true,
  /[A-Za-z0-9-_,\s]+\.yaml$/i
)
