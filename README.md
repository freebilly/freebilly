# FreeBilly

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/freebilly/freebilly/master) ![Gitlab code coverage](https://img.shields.io/gitlab/coverage/freebilly/freebilly/master) [![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

A lightweight and minimalist eBook server, with (upcoming) built-in OPDS support

## Development

### Front-end (webui)

#### Project setup

```
$ pnpm install
```

#### Compiling and hot-reloading for development

```
$ pnpm watch
```

#### Compiling and minifying for production

```
$ pnpm build
```

#### Linting and fixing files

```
$ pnpm lint
```

#### Updating dependencies

```
$ pnpm up
```

#### Customising configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Back-end (server)

#### Setting up

*Follow the appropriate instructions for your OS in order to install `pdm`.*

```
$ pdm install --dev
```

#### Initialising the database

```
$ pdm initdb
```

*(Additionally, the database can then be populated with fake data by running `pdm run populate_db.py`).*

#### Running the server

```
$ pdm start
```

#### Checking for dependency updates

```
$ pdm update --outdated
```

#### Updating dependencies

```
$ pdm update
```

#### Running tests

```
$ pdm test
```
